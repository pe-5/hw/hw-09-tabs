const tabList = document.querySelector('.tabList');
const contentList = document.querySelector('.contentList');
let currentTab = tabList.querySelector('.active');
let currentContent = contentList.querySelector('.active');

tabList.addEventListener("click", (e) => {
  currentTab.classList.remove("active");
  currentContent.classList.remove("active");

  currentTab = e.target;

  for (let key of contentList.children) {
    if (key.dataset.user === currentTab.dataset.user) {
      key.classList.add("active");
      currentContent = key;
    }
  }
  currentTab.classList.add("active");
})











// const tabs = document.querySelectorAll('.tab');
// const contents = document.querySelectorAll('.content');
// const all = document.querySelectorAll('li');

// tabs.forEach((tab, index) => {
//   tab.addEventListener("click", () => {
//     all.forEach((li) => li.classList.remove("active"));
//     tab.classList.add("active");
//     contents[index].classList.add("active");
//   })
// })

